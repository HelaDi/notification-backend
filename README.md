#Notification-Backend

##Virtual environment

###Install virtual environment

	$ pip install virtualenv

###Create new virtual environment
Use cd command navigate into your project folder, and then create a new virtual environment.

	$ virtualenv -p python2.7 env

###Activate virtual environment

	$ source env/bin/activate

###Deactivate virtual environment
This commond is for deactivate virtual environment after you finish all your develop operations.

	$ deactivate

##Install Django

	$ pip install django

##Install Django-rest-framework

	$ pip install djangorestframework

##Install markdown

	$ pip install markdown

##Install celery

	$ pip install django-celery

##Create a Django project

    $ django-admin.py startproject notification_backend

##Create a Django application

    $ cd notification_backend
    $ ./manage.py startapp notification_status_app

##PostgrSQL

###Install PostgreSQL

	$ sudo yum install postgresql-libs postgresql postgresql-server

###Initialize database

	$ service postgresql initdb

###Start/Stop/Restart PostgreSQL as a service

####Start:

	$ service postgresql start

####Stop:

	$ service postgresql stop

####Restart:

	$ service postgresql restart

###Add PostgreSQL service to the autostart

	$ sudo chkconfig postgresql on

###Connect to default postgres/Exit

####Connect:

	$ sudo -u postgres -i

####Exit:

	$ exit

###Create database
To Create database first step you need login postgres, and then use "psql" command switch to SQL edit model.

####1.Connect to default database postgres:

	$ sudo -u postgres -i

####2.Switch to SQL edit model

	$ psql

If quit SQL edit model

	# \q

####3.Create database

	# CREATE DATABASE notification_db;

###Create role is database as superuser with password

####1.Switch to SQL edit model in "notification_db"

	$ psql notification_db

####2.Create a role

	# CREATE USER notification_manager WITH SUPERUSER PASSWORD 'notification';

###Install psycopg2 for Django database engine

    $ sudo yum install gcc python-setuptools python-devel postgresql-devel
    $ sudo easy_install psycopg2

###Some userful database command
Drop all tables in database

    # DROP SCHEMA PUBLIC CASCADE;
    # CREATE SCHEMA PUBLIC;

Lookup data in a table
Example:

    # SELECT * FROM notification_status_app_notificationstatus;

###Test on local mechine

####Run local Django test server
Go to the folder contain manage.py first, and then run server.

    $ ./manage.py runserver

####Install Httpie in Macbook

    $ brew install httpie

####Test API

    $ http --verbose --debug POST http://127.0.0.1:8000/notification_status/many=false action_type=post notification_id=4098 package_name=com.tencent.mm time_stamp=1447226512143 type_flag=N user_hash_id=16f9a13b67322955f28248af218b5095
    $ http --verbose --debug POST http://127.0.0.1:8000/notification_status/many=true < test.json

##Deploy

###Install httpd and mod_wsgi

    $ pip install mod_wsgi-httpd

Download mod_wsgi and then

    $ tar -xvf *.tar
    $ cd mod*
    $ sudo yum install httpd-devel
    $ sudo python setup.py install

###httpd server commands

    $ service httpd start
    $ service httpd restart
    $ service httpd stop

##Usage

For upload one notification status:

url:

    http://ec2-54-218-7-156.us-west-2.compute.amazonaws.com/notification_status/many=false

json: 

    {
        "action_type": "post", 
        "notification_id": "4098", 
        "package_name": "com.tencent.mm", 
        "time_stamp": "1447226512143", 
        "type_flag": "N", 
        "user_hash_id": "16f9a13b67322955f28248af218b5095"
    }

For batch upload:

url:

    http://ec2-54-218-7-156.us-west-2.compute.amazonaws.com/notification_status/many=true

json:

    [
        {
            "action_type": "ScreenOff", 
            "time_stamp": "1447226291778", 
            "type_flag": "S", 
            "user_hash_id": "16f9a13b67322955f28248af218b5095"
        }, 
        {
            "action_type": "post", 
            "notification_id": "4098", 
            "package_name": "com.tencent.mm", 
            "time_stamp": "1447226512143", 
            "type_flag": "N", 
            "user_hash_id": "16f9a13b67322955f28248af218b5095"
        }
    ]
